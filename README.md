## Quick start

Pour lancer rapidement le code veuillez créer deux terminaux :

-exécutez ce code:

#### installer les dépendances
```
pip install -r requirements.txt

```
#### Expansion de requête et ranking

Le but de ce code est de répondre à une requête envoyée par l'utilisateur. Pour ce faire, le code effectue d'abord le traitement nécessaire pour la requête et filtre ensuite les documents qui ont un ou tous les tokens de la requête.

Ensuite, les documents filtrés sont utilisés pour attribuer un score à chaque document. Pour ce faire, le code a implémenté deux fonctions:

**La fonction calculate_score_occurances** qui prend en compte l'importance du nombre d'occurrences des tokens dans les documents.

**La fonction calculate_score_postions**  qui prend en compte la position des mots-clés et donne un score plus élevé aux documents qui correspondent à la position et applique un poids plus important aux tokens qui ont un sens par rapport aux stop words.
A la fin on obtient un fichier jresult.son qui contient :
- l'id des documents leurs Titres et leurs Urls
- Leurs scores qui sont calculés par la 'fonction calculate_score_postions' qui me semble plus pertinentes
- le nombre de documents dans l'index
- le nombre de documents ayant survécu au filtre.
### Pour lancer le code, il faut préciser la requête et le mode :

Le mode :
- **ET** si on veut que les documents qui ont tous les tokens de la requête.
- **OU** si on veut que les documents qui ont au moins un token de la requête.


```
python3 main.py --query "ce qui karine  lacombe" --mode OU
```
#### Pour tester:
```
python -m unittest 
```