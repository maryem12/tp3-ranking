import unittest
from ranking.filtrer_documents import *
import json
class TestCrawler(unittest.TestCase):
    def setUp(self):
        with open('index_unicoded.json', 'r') as f:
            self.index_unicoded = json.load(f)
   
    def test_filtered_documents_With_ET_True(self):
      #tester si la fonction prend compte tous  documents qui ont tous les tokens de la requête,
        query="Karine Lacombe"
        tokens=traitement(query)
        documents=filtered_documents(tokens,self.index_unicoded, mode='ET')
        self.assertEqual(documents, ["0"])

    def test_filtered_documents_With_ET_false(self):
      #tester si la fonction prend compte tous  documents qui ont tous les tokens de la requête
        query="Karine la Lacombe"
        tokens=traitement(query)
        documents=filtered_documents(tokens,self.index_unicoded, mode='ET')
        self.assertNotEqual(documents, ["0"])

    def test_filtered_documents_With_OU(self):
      #tester si la fonction au moins unprend en compte un token 
        query="Karine la Lacombe"
        tokens=traitement(query)
        documents=filtered_documents(tokens, self.index_unicoded, mode='OU')
        assert len(documents)>30
