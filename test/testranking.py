import unittest
from ranking.ranking import *
import json


class TestCalculateScorePositions(unittest.TestCase):
    def setUp(self):
        with open('index_unicoded.json', 'r') as f:
            self.index_unicoded = json.load(f)
    
    def test_calculate_score_positions(self):
        tokens=["karine","la","lacombe"]
        documents_filtred=["485", "82", "428", "407", "139", "241", "74", "458", "423", "135", "13", "472", "297", "66", "446", "456", "80", "411", "338", "221", "460", "234", "353", "41", "264", "33", "211", "272", "344", "426", "403", "376", "379", "365", "112", "486", "363", "0", "414", "126", "422", "79"]
        
        result =calculate_score_postions(self.index_unicoded,documents_filtred,tokens)
        print("score_with_postion",result)
    def test_calculate_score_occurances(self):
        tokens=["karine","la","lacombe"]
        documents_filtred=["485", "82", "428", "407", "139", "241", "74", "458", "423", "135", "13", "472", "297", "66", "446", "456", "80", "411", "338", "221", "460", "234", "353", "41", "264", "33", "211", "272", "344", "426", "403", "376", "379", "365", "112", "486", "363", "0", "414", "126", "422", "79"]
        
        result =calculate_score_occurances(tokens,self.index_unicoded,documents_filtred)
        print("score_with_occurances",result)
        
if __name__ == '__main__':
    unittest.main()
