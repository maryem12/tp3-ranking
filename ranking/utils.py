from unidecode import unidecode
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import json

def preprocessing(text):
    #pour unidecoder les mots 
    text=unidecode(text.lower())
    print(text)
    return text
def word_tokenizer(text):
    tokens = word_tokenize(text)
    return tokens
def stopWords(tokens):
    stop_words = set(stopwords.words())
    filtered_tokens = [token for token in tokens if token not in stop_words]
    return filtered_tokens


def preprocess_keys(filename):
    #cette focntion prend en entrer le fichier index et unicode les mots comme wikip\u00e9dia
    # qui  contiennent  \u00e9 au lieu de e et parsuite retourne un json unidecodé
    with open(filename, "r") as f:
        data = json.load(f)
        index_unicoded={}
    new_keys = []
    for key in data.keys():
        new_key =unidecode(key.lower())
        new_keys.append((new_key, data[key]))
    
    for new_key, value in new_keys:
        index_unicoded[new_key] = value
    
    return index_unicoded
def output(documents_sorted,documents):
    results={}
    results["metadata"]={}
    results["metadata"]["num_documents_in_index"] = len(documents)
    results["metadata"]["num_filtered_documents"] = len(documents_sorted)
    for i, score in  documents_sorted.items():
        for j in range(len(documents)):
            if str(i)==str(documents[j]['id']):
                results[str(i)]={}
                results[str(i)]['url']=documents[j]['url']
                results[str(i)]['title']=documents[j]['title']
                results[str(i)]['score']=score
    with open("output/results.json","w") as f:
        json.dump(results,f)